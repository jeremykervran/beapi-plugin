<?php

function beapi_annonces_query( $query ) {
	if ( $query->is_tax( 'edition' ) && $query->is_main_query() ) {
		$terms = get_terms( 'edition', array( 'fields' => 'ids' ) );
		$query->set( 'post_type', array( 'post' ) );
		$query->set( 'tax_query', array(
			'relation' => 'OR',
			array(
				'taxonomy' => 'edition',
				'field' => 'id',
				'terms' => $terms,
				'operator' => 'NOT IN'
			)
		) );
	}

	return $query;
}
add_filter( 'pre_get_posts', 'beapi_annonces_query' );