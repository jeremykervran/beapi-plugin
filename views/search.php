<?php

global $wp;
$current_url = get_home_url(null, $wp->request, null);

?>

<form role="search" method="get" action="<?php echo $current_url; ?>/">
	<input type="hidden" name="s" value="true" />

    <label>
        <input type="radio" name="radio-type" value="offres" />
        Offres
    </label>
    <label>
        <input type="radio" name="radio-type" value="demandes" />
        Demandes
    </label>

    <!-- Titre -->
    <label>
        <input type="text" name="titre" placeholder="Rechercher par titre"/>
    </label>

    <!-- Region -->
	<label for="region">Région
		<select name="region" id="search-select-categorie">
	        <?php
	        foreach (get_terms(['taxonomy' => 'region', 'hide_empty' => false]) as $region) {
	            ?>
	            <option value="<?php echo $region->slug; ?>"><?php echo $region->name; ?></option>
	            <?php
	        }
	        ?>
	    </select>
    </label>


	<!-- Catégorie -->
	<label for="categorie">Catégorie
		<select name="categorie" id="search-select-categorie">
		    <?php
		    foreach (get_terms(['taxonomy' => 'categorie', 'hide_empty' => false]) as $categorie) {
		        ?>
		        <option value="<?php echo $categorie->slug; ?>"><?php echo $categorie->name; ?></option>
		        <?php
		    }
		    ?>
		</select>
    </label>
    
	<input value="Rechercher" type="submit">
</form>