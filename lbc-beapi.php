<?php

/*
Plugin Name: leBonCoin BeApi
Plugin URI: https://github.com/jeremykervran/lbc-beapi/
Description: Reproduction de LeBonCoin pour BeApi
Author: Jeremy Kervran
Author URI: https://github.com/jeremykervran/
Text Domain: lbc-beapi
*/

define( 'LBC_VERSION', '1.0.0' );
define( 'LBC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

/**
 * Déclaration du CPT
 * @return void
 */
add_action( 'init', 'register_annonces_cpt' );
function register_annonces_cpt(): void {
	$labels = array(
		'name'                  => _x( 'Annonces', 'Post type general name', 'lbc-beapi' ),
		'singular_name'         => _x( 'Annonce', 'Post type singular name', 'lbc-beapi' ),
		'menu_name'             => _x( 'Annonces', 'Admin Menu text', 'lbc-beapi' ),
		'name_admin_bar'        => _x( 'Annonce', 'Add New on Toolbar', 'lbc-beapi' ),
		'add_new'               => __( 'Ajouter une annonce', 'lbc-beapi' ),
		'add_new_item'          => __( 'Ajouter une nouvelle annonce', 'lbc-beapi' ),
		'new_item'              => __( 'Nouvelle annonce', 'lbc-beapi' ),
		'edit_item'             => __( 'Editer l\'annonce', 'lbc-beapi' ),
		'view_item'             => __( 'Voir l\'annonce', 'lbc-beapi' ),
		'all_items'             => __( 'Toutes les annonces', 'lbc-beapi' ),
		'search_items'          => __( 'Recherche des annonces', 'lbc-beapi' ),
		'parent_item_colon'     => __( 'Annonces parentes :', 'lbc-beapi' ),
		'not_found'             => __( 'Aucune annonce trouvée', 'lbc-beapi' ),
		'not_found_in_trash'    => __( 'Aucune annonce trouvée dans la corbeille.', 'lbc-beapi' ),
		'featured_image'        => _x( 'Image de l\'annonce',
			'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'lbc-beapi' ),
		'set_featured_image'    => _x( 'Définir l\'image de l\'annonce',
			'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'lbc-beapi' ),
		'remove_featured_image' => _x( 'Supprimer l\'image de l\'annonce',
			'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'lbc-beapi' ),
		'use_featured_image'    => _x( 'Utiliser comme image pour l\'annonce',
			'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'lbc-beapi' ),
		'archives'              => _x( 'Archives des annonces',
			'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'lbc-beapi' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'annonces' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 5,
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
	);

	register_post_type( 'annonces', $args );
}

/**
 * Déclaration des champs ACF
 * @return void
 */
add_action( 'acf/init', 'register_annnonces_field_groups' );
function register_annnonces_field_groups(): void {
	if ( function_exists( 'acf_add_local_field_group' ) ) {
		acf_add_local_field_group( [
			'key'      => 'annonces_group',
			'title'    => 'Champs annonces',
			'fields'   => [
				[
					'key'   => 'field_1',
					'label' => 'Subtitle',
					'name'  => 'subtitle',
					'type'  => 'text',
				],
				[
					'key'     => 'field_2',
					'label'   => 'Select',
					'name'    => 'select',
					'type'    => 'select',
					'choices' => [
						'red'    => 'Red',
						'blue'   => 'Blue',
						'yellow' => 'Yellow',
					],
					'ui'      => 1,
					'ajax'    => 1,
				],
			],
			'location' => [
				[
					[
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'annonces',
					],
				],
			],
		] );
	}
}

/**
 * Déclarations des taxonomies : Types d'annonce, région et catégorie
 */
add_action( 'init', 'register_annonces_taxonomies' );
function register_annonces_taxonomies(): void {
	// Types d'annonce
	$labels = array(
		'name'              => _x( 'Types d\'annonce', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Type d\'annonce', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Rechercher dans les types d\'annonce', 'textdomain' ),
		'all_items'         => __( 'Tous les types d\'annonce', 'textdomain' ),
		'parent_item'       => __( 'Type parent', 'textdomain' ),
		'parent_item_colon' => __( 'Type parent:', 'textdomain' ),
		'edit_item'         => __( 'Editer le type', 'textdomain' ),
		'update_item'       => __( 'Mettre à jour le type', 'textdomain' ),
		'add_new_item'      => __( 'Ajouter un nouveau type', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de type', 'textdomain' ),
		'menu_name'         => __( 'Types d\'annonce', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'typeannonce' ),
	);

	register_taxonomy( 'typeannonce', array( 'annonces' ), $args );

	unset( $args );
	unset( $labels );

	// Regions
	$labels = array(
		'name'              => _x( 'Régions', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Région', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Rechercher dans les régions', 'textdomain' ),
		'all_items'         => __( 'Toutes les régions', 'textdomain' ),
		'parent_item'       => __( 'Région parente', 'textdomain' ),
		'parent_item_colon' => __( 'Région parente:', 'textdomain' ),
		'edit_item'         => __( 'Editer la région', 'textdomain' ),
		'update_item'       => __( 'Mettre à jour la région', 'textdomain' ),
		'add_new_item'      => __( 'Ajouter une nouvelle région', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de région', 'textdomain' ),
		'menu_name'         => __( 'Régions', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'region' ),
	);

	register_taxonomy( 'region', array( 'annonces' ), $args );

	unset( $args );
	unset( $labels );

	// Catégories
	$labels = array(
		'name'              => _x( 'Catégorie', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Catégorie', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Rechercher dans les catégories', 'textdomain' ),
		'all_items'         => __( 'Toutes les catégories', 'textdomain' ),
		'parent_item'       => __( 'Catégorie parente', 'textdomain' ),
		'parent_item_colon' => __( 'Catégorie parente:', 'textdomain' ),
		'edit_item'         => __( 'Editer la catégorie', 'textdomain' ),
		'update_item'       => __( 'Mettre à jour la catégorie', 'textdomain' ),
		'add_new_item'      => __( 'Ajouter une nouvelle catégorie', 'textdomain' ),
		'new_item_name'     => __( 'Nouveau nom de catégorie', 'textdomain' ),
		'menu_name'         => __( 'Catégories', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'categorie' ),
	);

	register_taxonomy( 'categorie', array( 'annonces' ), $args );

	unset( $args );
	unset( $labels );
}

